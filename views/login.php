<?php
session_start();
$alerta = ''; 
if (!empty($_SESSION['active'])) {
    header('location: theme/');
} else {
    if (!empty($_POST)) {
        if (empty(['username']) || empty(['pass'])) {
            $alerta = 'Ingrese su usuario y contraseña';
        } else {
            require_once '../connection/connection.php';
            $usuario = mysqli_real_escape_string($conection, $_POST['username']);
            $contraseña = mysqli_real_escape_string($conection, $_POST['password']);
            $query = mysqli_query($conection, "SELECT u.id_user,u.nombres,u.username,u.pass,r.id_rol,r.rol
                                               FROM usuarios u 
                                               INNER JOIN rol r 
                                                ON u.rol = r.id_rol 
                                               WHERE u.username = '$usuario' 
                                               AND u.pass ='$contraseña' AND estatus = 1");
            mysqli_close($conection);
            $resultado = mysqli_num_rows($query);
            if ($resultado > 0) {
                $data = mysqli_fetch_array($query);
                $_SESSION['active'] = true;
                $_SESSION['idUser'] = $data['id_user'];
                $_SESSION['nombre'] = $data['nombres'];
                $_SESSION['username'] = $data['username'];
                $_SESSION['rol'] = $data['id_rol'];
                $_SESSION['rol_name'] = $data['rol'];
                header("location: theme/");
            } else {
                $alerta = '<div class="alert">
                            Usuario o Contraseña Incorrecta
                         </div>';
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Play</title>
    <link rel="stylesheet" href="../libs/css/css/all.min.css">
    <link rel="stylesheet" href="../libs/css/fontawesome.min.css">
    <link rel="stylesheet" href="../libs/css/css/styless_.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="img/logo.png" type="image/x-icon">
</head>

<?php
if (!empty($_SESSION['active'])) {
    header('location: theme/');
} else {
    session_destroy();
?>

    <body>
        <div class="box">
            <div class="form">
                <form action="login.php" method="post">
                    <h2>Sing In</h2>
                    <div class="inputBox">
                        <input type="text" name="username" required="required">
                        <span>Username</span>
                        <i></i>
                    </div>
                    <div class="inputBox">
                        <input type="password" class="usr-pass" name="password" required="required">
                        <span>Password</span>
                        <i></i>
                    </div>
                    <div class="links">
                        <a href="#">Olvidaste tu Constraseña</a>
                        <a href="signup.php">Sign Up</a>
                    </div>
                    <div class="alerta"><?php echo isset($alerta) ? $alerta : '' ?></div>
                    <div class="buttons">
                        <input type="submit" class="btn btn-primary cerrar" name="login" value="Iniciar Sesión">
                        <button><a href="../index.php">Regresar</a></button>
                    </div>
                </form>
            </div>
        </div>
        <script src="../libs/js/all.min.js"></script>
        <script src="../libs/js/fontawesome.min.js"></script>
    </body>

</html>
<?PHP
}
?>