<?php 
  session_start();
  if (!isset($_SESSION['username'])) {
    header('Location: ../index.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Crucigrama</title>
    <link rel="stylesheet" href="styles.css" />
  </head>
  <br>
  <body>
    <div id="container">
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
    </div>
    <br /><br />
    <div id="info">
      <div id="input-info">
        <div id="input-string">Click para juegar</div>
        <div id="alphabetic-keys">
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>A</b></span>
          </div>
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>B</b></span>
          </div>
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>C</b></span>
          </div>
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>D</b></span>
          </div>
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>E</b></span>
          </div>
          <div class="alphabetic-key">
            <img src="alphabet key.jpg" alt="" />
            <span><b>F</b></span>
          </div>
        </div>
        <div id="other-keys">
          <div id="space-key">
            <img src="space key.jpg" alt="" />
            <span><b>Barra Espaciadora</b></span>
          </div>
          <div id="backspace-key">
            <img src="backspace key.jpg" alt="" />
            <span><b>Borrar</b></span>
          </div>
        </div>
      </div>
      <div id="timer">
        <div id="clock">
          <span id="countdown">300</span>
        </div>
      </div>
      <div id="score">
        <span id="scoreText">PUNTAJE: <span id="scoreValue">0</span></span>
      </div>
    </div>
    <br />
    <audio src="bgMusic.mp3" id="bgMusic" loop></audio>
  </body>
  <script src="varGlobals.js"></script>
  <script src="eventListener.js"></script>
  <script src="funtiones.js"></script>
</html>
