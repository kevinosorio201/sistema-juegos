<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scale=no,initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Rompecabezas Facil</title>
        <link rel="stylesheet" href="style_pzl.css">
        <script src="rompecabezas.js"></script>
    </head>

    <body>
        <img id="title" src="./logo_ciencias.png">
        <div id="board">
        </div>
        <div class="muestra">
            <h4 id="text">Modelo de rompecabezas a resolver</h4>
            <img id="muestra" src="./10.png">
        </div>
        
        <h1>Turns: <span id="turns">0</span></h1>
    </body>
</html>