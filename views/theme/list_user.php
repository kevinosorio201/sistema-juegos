<?php
session_start();

if ($_SESSION['rol'] != 1) {
    header('location: ../views/theme/index.php');
}
include_once '../../connection/connection.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lista Usuarios </title>

    <?php include_once 'layouts/header.php' ?>
    <style>
        .activar {
            color: white;
            border: 1px solid #17A2B8;
            background: #17A2B8;
            display: inline-block;
        }

        .activar::before {
            background: #17A2B8;
        }

        .activar::after {
            background: #17A2B8;
        }

        .activar a:hover {
            background: #17A2B8;
        }
    </style>

    <?php include_once 'layouts/nav.php' ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Lista de Usuarios</b></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
                            <li class="breadcrumb-item active">Usuarios</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-12" style="margin-bottom: 5px; margin-left: -23px;">
                            <form action="search_users.php" method="get" class="form-inline ml-3">
                                <div class="input-group input-group-sm col-md-5">
                                    <input class="form-control form-control-navbar" type="search" placeholder="Buscar Usuario" aria-label="Search" name="search" id="search">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-info" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-inverse ">
                                    <thead class="bg-info thead-inverse">
                                        <tr class="text-center">
                                            <th scope="col">ID</th>
                                            <th scope="col">NOMBRES</th>
                                            <th scope="col">USUARIO</th>
                                            <th scope="col">ROL</th>
                                            <th scope="col">ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    // Paginador
                                    $sql_register = mysqli_query($conection, "SELECT COUNT(*) as total_registro FROM usuarios WHERE estatus = 1");
                                    $result_register = mysqli_fetch_array($sql_register);
                                    $total_registro = $result_register['total_registro'];

                                    $paginas = 5;

                                    if (empty($_GET['pagina'])) {
                                        $pagina = 1;
                                    } else {
                                        $pagina = $_GET['pagina'];
                                    }

                                    $desde = ($pagina - 1) * $paginas;
                                    $total_paginas = ceil($total_registro / $paginas);

                                    $query = mysqli_query($conection, "SELECT u.id_user, u.nombres, u.username, r.rol 
                                                                FROM usuarios u INNER JOIN rol r ON u.rol = r.id_rol 
                                                                WHERE estatus = 1 ORDER BY u.id_user 
                                                                ASC LIMIT $desde, $paginas");
                                    mysqli_close($conection);
                                    $result = mysqli_num_rows($query);
                                    if ($result > 0) {
                                        while ($data = mysqli_fetch_array($query)) {
                                    ?>
                                            <tbody>
                                                <tr class="text-center">
                                                    <td><?php echo $data['id_user']; ?></td>
                                                    <td><?php echo $data['nombres']; ?></td>
                                                    <td><?php echo $data['username']; ?></td>
                                                    <td><?php echo $data['rol']; ?></td>
                                                    <td>
                                                        <a href="edit_user.php?id=<?php echo $data['id_user']; ?>" class="btn btn-warning"><i class="fas fa-edit"></i> </a>
                                                        <?php
                                                        if ($data['id_user'] != 1) {
                                                        ?>
                                                            <a href="delete_user.php?id=<?php echo $data['id_user']; ?>" class="btn btn-danger"><i class="fas fa-trash-alt"></i> </a>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                    <?php
                                        }
                                    }
                                    ?>
                                </table>

                                <div>
                                    <nav aria-label="...">
                                        <ul class="pagination justify-content-end">
                                            <?php
                                            if ($pagina != 1) {
                                                # code...

                                            ?>
                                                <li class="page-item">
                                                    <a class="page-link" href="?pagina=<?php echo 1; ?>"><i class="nav-icon fas fa-backward-step"></i></a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="?pagina=<?php echo $pagina - 1; ?>" aria-label="Previous">
                                                        <span aria-hidden="true"><i class="nav-icon fas fa-backward-fast"></i></span>
                                                    </a>
                                                </li>
                                            <?php
                                            }
                                            for ($i = 1; $i <= $total_paginas; $i++) {
                                                # code...
                                                if ($i == $pagina) {
                                                    echo '  <li class="page-link activar">' . $i . '</li>';
                                                } else {
                                                    echo '  <li class="page-item">
                                                            <a class="page-link" href="?pagina=' . $i . '">' . $i . '</a>
                                                        </li>';
                                                }
                                            }
                                            if ($pagina != $total_paginas) {
                                                # code...

                                            ?>
                                                <li class="page-item">
                                                    <a class="page-link" href="?pagina=<?php echo $pagina + 1; ?>" aria-label="Next">
                                                        <span aria-hidden="true"><i class="nav-icon fas fa-forward-fast"></i></span>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="?pagina=<?php echo $total_paginas; ?>"><i class="nav-icon fas fa-forward-step"></i></a>
                                                </li>

                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include_once 'layouts/footer.php' ?>

</html>