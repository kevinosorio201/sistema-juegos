<?php
session_start();
if ($_SESSION['rol'] != 1) {
    header('location: ../views/theme/index.php');
}

include_once '../../connection/connection.php';
if (!empty($_POST)) {
    $alerta = '';

    if (empty($_POST['names']) || empty($_POST['users']) || empty($_POST['rol'])) {
        $alerta = '<p class="msg_error">Todos los campos son obligatorios</p>';
    } else {
        $id = $_POST['id'];
        $names = $_POST['names'];
        $users = $_POST['users'];
        $pass = $_POST['pass'];
        $rol = $_POST['rol'];

        $query = mysqli_query($conection, "SELECT * FROM usuarios WHERE (username = '$users' AND id_user != $id) 
                                            OR (nombres = '$names' AND id_user != $id)");
        $result = mysqli_fetch_array($query);
        if ($result > 0) {
            $alerta = '<p class="msg_error">El nombre o usuario ya existe</p>';
        } else {
            if (empty($_POST['pass'])) {
                $sql_update = mysqli_query($conection, "UPDATE usuarios SET nombres = '$names', username = '$users', 
                                                        rol = '$rol' WHERE id_user = $id");
            } else {
                $sql_update = mysqli_query($conection, "UPDATE usuarios SET nombres = '$names', username = '$users', 
                                                        pass = '$pass', rol = '$rol' WHERE id_user = $id");
            }
            if ($sql_update) {
                $alerta = '<p class="msg_save">Usuario actualizado correctamente</p>';
            } else {
                $alerta = '<p class="msg_error">Error al actualizar usuario</p>';
            }
        }
    }
}

//recuperar datos del usuario
if (empty($_REQUEST['id'])) {
    header('location: list_user.php');
}
$id = $_REQUEST['id'];

$query = mysqli_query($conection, "SELECT * FROM usuarios WHERE id_user = $id AND estatus = 1");
$result = mysqli_num_rows($query);
if ($result == 0) {
    header('location: list_user.php');
} else {
    while ($data = mysqli_fetch_array($query)) {
        $id = $data['id_user'];
        $names = $data['nombres'];
        $users = $data['username'];
        $pass = $data['pass'];
        $rol = $data['rol'];
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar Usuario </title>

    <?php include_once 'layouts/header.php' ?>

    <?php include_once 'layouts/nav.php' ?>

    <style>
        .msg_error {
            color: #BD2130;
        }

        .msg_save {
            color: #28A745;
        }

        .alerta p {
            padding: 10px;
        }

        .notItemOne option:first-child {
            display: none;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Actualizar datos del usuario</b></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
                            <li class="breadcrumb-item active">Editar Usuario</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="row">
                <div class="col-sm-8">
                    <div class="col-md-12" style="padding-bottom: 5px">
                        <ul class="nav justify-content-end" style="margin-right: -7px;">
                            <li class="nav-item">
                                <a class="btn btn-block bg-danger" href="list_user.php"><i class="nav-icon fas fa-arrow-left"></i> Regresar</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="text-center" style="font-size: 20px;"><b>Actualizar datos</b></h3>
                        </div>
                        <div class="card-body">
                            <div class="alerta text-center"><?php echo isset($alerta) ? $alerta : '';  ?></div>
                            <form action="" class="form-horizontal" method="POST">
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="names" class="col-sm-3 col-form-label">Nombres del usuario</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="names" id="names" class="form-control" value="<?php echo $names; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="users" class="col-sm-3 col-form-label">Actualice el usuario</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="users" id="users" class="form-control" value="<?php echo $users; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pass" class="col-md-4 col-form-label">Actualice la contraseña</label>
                                    <div class="col-sm-7" style="margin-left: -55px;">
                                        <input type="password" name="pass" id="pass" class="form-control" value="<?php echo $pass; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="rol" class="col-sm-3 col-form-label">Tipo de usuario</label>
                                    <div class="col-sm-7">
                                        <select name="rol" id="rol" class="form-control">
                                            <option value="1" <?php
                                                                if ($rol == 1) {
                                                                    echo "selected";
                                                                }
                                                                ?>>Administrador</option>
                                            <option value="2" <?php
                                                                if ($rol == 2) {
                                                                    echo "selected";
                                                                }
                                                                ?>>Juegador</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-7 float-right">
                                        <input type="submit" class="btn btn-block btn-outline-warning" value="Actualizar Usuario">
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
    </div>

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include_once 'layouts/footer.php' ?>

</html>