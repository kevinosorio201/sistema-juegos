<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rompecabezas 5x5</title>
    <link rel="stylesheet" href="puzzless.css">
    <script src="puzzles.js"></script>
</head>

<body>
    <img id="title" src="images/logo_ciencias.png" alt="">
    <div id="board"></div>
    <div id="muestra">
        <img src="images/26.png">
    </div>
    <br><br><br>
    <h2>Turns: <span id="turns">0</span></h2>
    <div id="pieces"></div>
</body>

</html>