<?php
session_start();
if ($_SESSION['rol'] != 1) {
    header('location: ../views/theme/index.php');
}

include_once '../../connection/connection.php';
if (!empty($_POST)) {
    if ($_POST['id'] == 1) {
        header('location: list_user.php');
        mysqli_close($conection);
        exit;
    }
    $id = $_POST['id'];

    $queryD = mysqli_query($conection, "UPDATE usuarios SET estatus = 0 WHERE id_user = $id");
    mysqli_close($conection);
    if ($queryD) {
        header('location: list_user.php');
    } else {
        echo "Error al eliminar";
    }
}

//recuperar datos del usuario
if (empty($_REQUEST['id']) || $_REQUEST['id'] == 1) {
    header('location: list_user.php');
    mysqli_close($conection);
} else {
    $id = $_REQUEST['id'];

    $query = mysqli_query($conection, "SELECT * FROM usuarios WHERE id_user = $id AND estatus = 1");
    $result = mysqli_num_rows($query);
    if ($result == 0) {
        header('location: list_user.php');
    } else {
        while ($data = mysqli_fetch_array($query)) {
            $id = $data['id_user'];
            $names = $data['nombres'];
            $users = $data['username'];
            $pass = $data['pass'];
            $rol = $data['rol'];
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eliminar Usuario </title>

    <?php include_once 'layouts/header.php' ?>

    <?php include_once 'layouts/nav.php' ?>

    <style>
        .rows {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100px;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Eliminar datos del usuario</b></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
                            <li class="breadcrumb-item active">Eliminar Usuario</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="rows">

                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="text-center" style="font-size: 20px; margin-top: 10px;"><b>Elimnar datos del Usuario</b></h3>
                    </div>
                    <div class="card-body text-center">
                        <h5><strong>¿Esta seguro que desea eliminar el registro?</strong></h5>
                        <p><b>Nombres: </b><span class="badge bg-dark"> <?php echo $names ?></span></p>
                        <p><b>Usuario: </b><span class="badge bg-dark"> <?php echo $users ?></span></p>
                        <p><b>Rol: </b><span class="badge bg-dark"> <?php echo $rol ?></span></p>
                    </div>
                    <div class="card-footer">
                        <div class="class=" buttons" style="text-align: center;">
                            <form action="" method="POST">
                                <input type="hidden" name="id" value="<?php echo $id ?>">
                                <button style="margin-left: 0;" type="submit" class="btn bg-danger boton"><i class="nav-icon fas fa-trash"></i> Eliminar</button>
                                <button style="margin-left: 135px;" class="btn bg-secondary boton"><i class="nav-icon fas fa-ban"></i><a href="list_user.php"> Cancelar</a></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include_once 'layouts/footer.php' ?>

</html>