<?php
session_start();
if (empty($_SESSION['active'])) {
    header('location: ../login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard | SYS PLAYS </title>

    <?php include_once 'layouts/header.php' ?>

    <?php include_once 'layouts/nav.php' ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Juegos</b></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active">Juegos</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="row">
                <div class="col-sm-4">
                    <div class="card bg-success">
                        <img src="../theme/utils/img/adivina-2_1.png" class="card-img-top" style="width: 230px;margin-left: 50px;margin-top: 5px;" alt="ahorcado">
                        <div class="card-body">
                            <h2 class="text-center"><a class="text-white" href="ahorcado.php"><b>Adivina la palabra</b></a></h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card bg-warning">
                        <img src="../theme/utils/img/memories.jpg" class="card-img-top" style="width: 230px;margin-left: 50px;margin-top: 5px;border-radius: 10px;" alt="ahorcado">

                        <div class="card-body">
                            <h2 class="text-center"><a href="memories.php" class="text-dark"><b>Memoriza la imagen</b></a></h2>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card bg-primary">
                        <img src="../theme/utils/img/questions.webp" class="card-img-top" style="width: 210px;margin-left: 85px;margin-top: 5px;border-radius: 10px;" alt="ahorcado">

                        <div class="card-body">
                            <h3 class="text-center"><a href="quiz/" class="text-white"><b>Pon a prueba tus conocimientos</b></a></h3>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="card bg-dark">
                        <img src="../theme/utils/img/rmp.webp" class="card-img-top" style="width: 178px;margin-left: 85px;margin-top: 5px;border-radius: 10px;" alt="ahorcado">

                        <div class="card-body">
                            <h3 class="text-center"><a href="crosswords/index.php" class="text-white"><b>Completa el crucigrama</b></a></h3>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card bg-danger">
                        <img src="../theme/utils/img/cr.jpg" class="card-img-top" style="width: 210px;margin-left: 85px;margin-top: 5px;border-radius: 10px;" alt="ahorcado">

                        <div class="card-body">
                            <h3 class="text-center"><a href="puzle.php" class="text-white"><b>Divierte con el rompecabezas</b></a></h3>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include_once 'layouts/footer.php' ?>

</html>