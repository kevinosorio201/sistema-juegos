<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 1.0.1
    </div>
    <strong>Copyright &copy; 2022-2023 <a href="">Desarrollado por Estudiantes Universitarios</a>.</strong> Todos los Derechos Reservados.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="utils/js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="utils/js/all.min.js"></script>
<script src="utils/js/bootstrap.bundle.min.js"></script>
<script src="utils/js/fontawesome.min.js"></script>
<script src="utils/js/kitfont.js"></script>

<!-- AdminLTE App -->
<script src="utils/js/adminlte.min.js"></script>
</body>