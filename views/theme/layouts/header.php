<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="utils/css/css/all.min.css">
<link rel="stylesheet" href="utils/css/fontawesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="utils/css/adminlte.min.css">

<link rel="shortcut icon" href="utils/img/logo.png" type="image/x-icon">

</head>



<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="index.php" class="nav-link">Inicio</a>
                </li>
            </ul>

            <?php
            date_default_timezone_set('America/Guayaquil');

            function fechaC()
            {
                $mes = array(
                    "", "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                );
                return date('d') . " de " . $mes[date('n')] . " de " . date('Y');
            }
            ?>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <p style="padding: 7px;"><b>La Maná, <?php echo fechaC(); ?> </b></p>
                </li>
                <li class="nav-item">

                    <a href="../../core/salir.php" class="btn bg-danger text-center"><b>Cerrar Sesion <i class="fa-solid fa-right-from-bracket"></i></b></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->