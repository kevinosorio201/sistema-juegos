<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
        <img src="" alt="Imagen" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">SYS PLAYS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="utils/img/user.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?php echo $_SESSION['username'] . ' | ' . $_SESSION['rol_name'] ?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <?php if ($_SESSION['rol'] == 1) { ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Usuarios
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="list_user.php" class="nav-link">
                                    <i class="fas fa-list text-primary nav-icon"></i>
                                    <p>Lista Usuarios</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-play"></i>
                        <p>
                            Juegos
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../theme/ahorcado.php" class="nav-link">
                                <i class="fa-sharp fa-skull-crossbones nav-icon"></i>
                                <p>Ahorcado</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../theme/memories.php" class="nav-link">
                                <i class="fa-solid fa-gamepad nav-icon"></i>
                                <p>Memorias</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../theme/quiz/" class="nav-link">
                                <i class="fa-solid fa-question nav-icon"></i>
                                <p>Preguntas</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../theme/crosswords/index.php" class="nav-link">
                                <i class="nav-icon"><img src="utils/img/crucigrama.png"></i>
                                <p>Crucigrama</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../theme/puzle.php" class="nav-link">
                                <i class="fa-solid fa-puzzle-piece nav-icon"></i>
                                <p>Rompecabezas</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>