<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rompecabezas</title>

    <?php include_once 'layouts/header.php' ?>

    <?php include_once 'layouts/nav.php' ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Juego de rompecabezas</b></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active">Rompecabezas</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <h4 class="text-center">
                <b>El juego cuenta con dos niveles de dificultad,
                    el primero es de 3x3 y el segundo de 5x5</b>
            </h4>
            <div class="row">
                <div class="col-sm-3">

                </div>
                <div class="col-sm-3">
                    <div class="card bg-info">
                        <div class="card-body">
                            <h3 class="text-center">Nivel basico</h3>
                            <a href="../theme/slide-puzzle/index.php?nivel=basico" class="btn btn-success btn-block">Jugar</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card bg-danger">
                        <div class="card-body">
                            <h3 class="text-center">Nivel avanzado</h3>
                            <a href="../theme/Puzzle-Game/index.php?nivel=avanzado" class="btn btn-success btn-block">Jugar</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include_once 'layouts/footer.php' ?>

</html>