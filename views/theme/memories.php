<?php 
session_start();

if (!isset($_SESSION['username'])) {
    header('Location: ../index.php');
    exit();
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Juego de Memoria</title>


    <style>
        .body {
            background: lightblue;
            font-size: 16px;
        }

        .grid {
            display: flex;
            flex-wrap: wrap;
            margin-top: 20px;
        }

        .data-id {
            background: lightblue;
            width: 150px;
            height: 100px;
            border: 4px solid black;
        }

        .button {
            margin: 20px;
            background: green;
            border: none;
            color: #FFF;
            font-size: 2em;
            border-radius: 10px;
        }

        .button:hover {
            border: 2px solid white;
            background: black;
        }

        .span {
            border: 3px solid #000;
            padding: 4px;
            background: lightblue;
            margin-right: 100px;
            font-size: 12px;
            display: block;
        }

        .minspan {
            display: inline-block;
            margin: 10px 0px;
        }

        .contenido {
            font-size: 20px;
            font-weight: bold;
            padding: 1em;
            font-family: 'Courier New', Courier, monospace;
            background: white;
            border: 2px solid black;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100px;
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -7.5px;
            margin-left: -7.5px;
        }

        .col-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .grid2 {
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            grid-gap: 10px;
            grid-auto-rows: minmax(50px, auto);
        }

        .btn {
            background: #007bff;
            border: none;
            padding: 10px;
            border-radius: 5px;
            cursor: pointer;
        }

        .btn:hover {
            background: #0056b3;
        }

        .btn:focus {
            outline: none;
        }

        .btn-primary {
            background: #007bff;
        }
        a{
            text-decoration: none;
            color: white;
            font-weight: 600;
            font-size: 18px;
            font-family: 'Courier New', Courier, monospace;
        }
    </style>

</head>

<body>
    <div class="body">
        <div class="contenido">
            <div class="minspan">Resultado::<span class="span" id="result"></span> </div>
            <div class="minspan">Parejas::<span class="span" id="couples"></span> </div>
            <div class="minspan">Tiempo::<span class="span" id="time"></span></div>
        </div>


        <div class="grid" id="grid">
        </div>
        <button class="button" onclick="LoadGame(4)">Facil</button>
        <button class="button" onclick="LoadGame(8)">Normal</button>
        <button class="button" onclick="LoadGame(12)">Dificil</button>
        <button class="button" onclick="LoadGame(16)">Extremo</button>

        <script>
            document.addEventListener('DOMContentLoaded', () => {

                const cardsAvailables = [{
                        name: 'fries',
                        img: 'utils/img/imgs/fries.png'
                    },
                    {
                        name: 'cheeseburger',
                        img: 'utils/img/imgs/cheeseburger.png'
                    },
                    {
                        name: 'ice-cream',
                        img: 'utils/img/imgs/ice-cream.png'
                    },
                    {
                        name: 'pizza',
                        img: 'utils/img/imgs/pizza.png'
                    },
                    {
                        name: 'milkshake',
                        img: 'utils/img/imgs/milkshake.png'
                    },
                    {
                        name: 'hotdog',
                        img: 'utils/img/imgs/hotdog.png'
                    },
                    {
                        name: '8-ball',
                        img: 'utils/img/imgs/8-ball.png'
                    },
                    {
                        name: 'brujula',
                        img: 'utils/img/imgs/brujula.png'
                    },
                    {
                        name: 'card',
                        img: 'utils/img/imgs/card.png'
                    },
                    {
                        name: 'dominoe',
                        img: 'utils/img/imgs/dominoe.png'
                    },
                    {
                        name: 'hand',
                        img: 'utils/img/imgs/hand.png'
                    },
                    {
                        name: 'marihuana',
                        img: 'utils/img/imgs/marihuana.png'
                    },
                    {
                        name: 'pig',
                        img: 'utils/img/imgs/pig.png'
                    },
                    {
                        name: 'shoes',
                        img: 'utils/img/imgs/shoes.png'
                    },
                    {
                        name: 'watch',
                        img: 'utils/img/imgs/watch.png'
                    },
                    {
                        name: 'x',
                        img: 'utils/img/imgs/x.png'
                    }
                ]

                const grid = document.querySelector('.grid')
                const resultDisplay = document.querySelector('#result')
                const couplesDisplay = document.querySelector('#couples')
                const timeDisplay = document.querySelector('#time')

                var cardsChosen
                var cardsWon
                var cardArray
                var countFails, countTime
                var Timer


                CleanBoard = () => {
                    var cell = document.getElementById("grid");

                    if (cell.hasChildNodes()) {
                        console.log(cell.childNodes.length)
                        while (cell.childNodes.length >= 1) {
                            cell.removeChild(cell.firstChild);
                        }
                    }
                }

                LoadGame = (End) => {

                    cardsChosen = []
                    cardsWon = []
                    cardArray = []
                    countFails = 0
                    countTime = 180

                    //Eraser all node child of grid, clean board
                    CleanBoard()

                    //load board of game End:4,8,12,16
                    cardsAvailables.forEach(element => {
                        if (cardArray.length / 2 < End) {
                            cardArray.push(element)
                            cardArray.push(element)
                        }
                    });

                    clearInterval(Timer)

                    cardArray.sort(() => 0.5 - Math.random())

                    Timer = setInterval(() => {
                        timeDisplay.textContent = countTime--

                        if (countTime < 0) {
                            resultDisplay.textContent = 'Fallaste, Perdiste el Juego'
                            clearInterval(Timer)
                            CleanBoard()
                        }
                    }, 1000)

                    createBoard()

                }
                //create your board
                function createBoard() {
                    for (let i = 0; i < cardArray.length; i++) {
                        var card = document.createElement('img')
                        card.classList.add('data-id')
                        card.setAttribute('data-id', i)
                        card.setAttribute("src", 'utils/img/imgs/white.png')
                        card.addEventListener('click', flipCard)
                        grid.appendChild(card)
                    }
                }

                //check for matches
                function checkForMatch() {
                    var cards = document.querySelectorAll('img')
                    const OneId = cardsChosen[0].id
                    const TwoId = cardsChosen[1].id

                    if (cardsChosen[0].name === cardsChosen[1].name) {
                        couplesDisplay.textContent = 'La pareja es correcta'
                        cardsWon.push(cardsChosen)
                        countFails = 0

                    } else {
                        couplesDisplay.textContent = ++countFails + ' Fails'
                        cards[OneId].setAttribute("src", 'utils/img/imgs/white.png')
                        cards[TwoId].setAttribute("src", 'utils/img/imgs/white.png')
                        cards[OneId].addEventListener('click', flipCard)
                        cards[TwoId].addEventListener('click', flipCard)

                    }
                    cardsChosen = []
                    resultDisplay.textContent = cardsWon.length
                    if (cardsWon.length === cardArray.length / 2) {
                        resultDisplay.textContent = 'Felicidades! Has terminado el juego!'
                        clearInterval(Timer)
                        CleanBoard()
                    }
                }


                function flipCard() {
                    var cards = document.querySelectorAll('img')
                    var cardId = this.getAttribute('data-id')
                    cardsChosen.push({
                        name: cardArray[cardId].name,
                        id: cardId
                    })
                    cards[cardId].removeEventListener('click', flipCard)
                    this.setAttribute('src',
                        cardArray[cardId].img)
                    if (cardsChosen.length > 1) {
                        setTimeout(checkForMatch, 200)
                    }
                }


            })
        </script>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="grid2">
                    <button class="btn btn-primary"><a href="index.php">Regresar</a></button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>