<?php
require '../connection/connection.php';
if (!empty($_POST)) {
    $alerta = '';

    if (
        empty($_POST['name']) || empty($_POST['username']) || empty($_POST['password'])
        || empty($_POST['rol'])
    ) {
        $alerta = '<p class="msg_error">Todos los campos son obligatorios.</p>';
    } else {
        $name = $_POST['name'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $rol = $_POST['rol'];

        $query = mysqli_query($conection, "SELECT * FROM usuarios WHERE nombres = '$name' OR username = '$username'");
        $resultado = mysqli_num_rows($query);
        if ($resultado > 0) {
            $alerta = '<p class="msg_error">El nombre o usuario ya existe.</p>';
        } else {
            $query_insert = mysqli_query($conection, "INSERT INTO usuarios(nombres, username, pass,rol) 
                                        VALUES('$name', '$username', '$password', '$rol')");
            if ($query_insert) {
                $alerta = '<p class="msg_save">Usuario creado correctamente.</p>';
            } else {
                $alerta = '<p class="msg_error">Error al registrar usuario.</p>';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-Up Users</title>
    <link rel="stylesheet" href="../libs/css/css/styles2_1.css">
    <link rel="shortcut icon" href="img/logo.png" type="image/x-icon">
</head>

<body>
    <div class="box">
        <div class="form">
            <form action="signup.php" method="post">
                <h2>Sign Up</h2>
                <div class="inputBox">
                    <input type="text" name="name" required="required">
                    <span>Names</span>
                    <i></i>
                </div>
                <div class="inputBox">
                    <input type="text" name="username" required="required">
                    <span>Username</span>
                    <i></i>
                </div>
                <div class="inputBox">
                    <input type="password" name="password" required="required">
                    <span>Password</span>
                    <i></i>
                </div>
                <div class="select">
                    <?php
                    $query_rol = mysqli_query($conection, "SELECT * FROM rol");
                    $result_rol = mysqli_num_rows($query_rol);
                    ?>
                    <span>Tipo de Usuario</span>
                    <select name="rol" id="rol" required="required">
                        <?php
                        if ($result_rol > 0) {
                            while ($rol = mysqli_fetch_array($query_rol)) {
                        ?>
                                <option value=" <?php echo $rol['id_rol']; ?>"><?php echo $rol['rol']; ?></option>
                        <?php

                            }
                        }
                        ?>
                    </select>
                    <i></i>
                </div>
                <?php if (!empty($alerta)) : ?>
                    <p class="alerta"><?= $alerta ?></p>
                <?php endif; ?>
                <div class="buttons">
                    <input type="submit" name="signup" value="Sign Up">
                    <button><a href="login.php">Login</a></button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>