<!-- jQuery -->
<script src="libs/js/jquery.min.js"></script>
<!-- jQuery Easing --> 
<script src="libs/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="libs/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="libs/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="libs/js/jquery.stellar.min.js"></script>
<!-- Carousel -->
<script src="libs/js/owl.carousel.min.js"></script>
<!-- Flexslider -->
<script src="libs/js/jquery.flexslider-min.js"></script>
<!-- countTo -->
<script src="libs/js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="libs/js/jquery.magnific-popup.min.js"></script>
<script src="libs/js/magnific-popup-options.js"></script>
<!-- Count Down -->
<script src="libs/js/simplyCountdown.js"></script>
<!--Font Awesome-->
<script src="libs/js/fontawesome.min.js"></script>
<script src="libs/js/all.min.js">
</script>
<!-- Main -->
<script src="libs/js/main.js"></script>
<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>