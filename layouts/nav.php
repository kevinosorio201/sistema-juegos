<div class="fh5co-loader"></div>

<div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="index.php"><i class="icon-study"></i>UEMG<span>.</span></a></div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="active"><a href="index.php">Inicio</a></li>
                            <li><a href="courses.php">Cursos</a></li>
                            <li><a href="teacher.php">Profesores</a></li>
                            <li class="btn-cta"><a href="views/login.php"><span>Login</span></a></li>
                            <li class="btn-cta"><a href="views/signup.php"><span>Sing-Up</span></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </nav>