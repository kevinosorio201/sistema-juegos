<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">

<!-- Animate.css -->
<link rel="stylesheet" href="libs/css/animate.css">
<!-- Icomoons Icon Fonts-->
<link rel="stylesheet" href="libs/css/icomoon.css">
<!-- Themify Icons-->
<link rel="stylesheet" href="libs/css/css/all.min.css">
<link rel="stylesheet" href="libs/css/fontawesome.min.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="libs/css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="libs/css/magnific-popup.css">

<!-- Owl Carousel  -->
<link rel="stylesheet" href="libs/css/owl.carousel.min.css">
<link rel="stylesheet" href="libs/css/owl.theme.default.min.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="libs/css/flexslider.css">

<!-- Pricing -->
<link rel="stylesheet" href="libs/css/pricing.css">

<!-- Theme style  -->
<link rel="stylesheet" href="libs/css/style.css">

<!-- Modernizr JS -->
<script src="libs/js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<script src="libs/js/respond.min.js"></script>